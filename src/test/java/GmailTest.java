import allure.CustomListener;
import bo.HomePageBO;
import bo.LoginPageBO;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import parser.CSVParser;
import utils.PropertyUtil;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Iterator;

@Listeners({CustomListener.class})
public class GmailTest extends BaseTest{
    private String userDataPath = PropertyUtil.configList.get("csv_path");
    private int letterNumber=1;

    @DataProvider(parallel = true)
    private Iterator<Object[]> loginDataCSV() {
        return CSVParser.parseCSVFile(new File(userDataPath)).iterator();
    }

    @Test(dataProvider = "loginDataCSV")
    public void markAsImportantTest(String email, String password) throws MalformedURLException {
        LoginPageBO loginPageBO = new LoginPageBO();
        HomePageBO homePageBO = new HomePageBO();
        loginPageBO.logIn(email, password);
        String headerBeforeImpMark = homePageBO.markMessAsImportant(letterNumber);
        String headerAfterImpMark = homePageBO.getMsgHeaderText();
        Assert.assertNotEquals(headerBeforeImpMark, headerAfterImpMark);
    }
}
