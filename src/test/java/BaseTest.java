import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import utils.DriverManager;

import java.net.MalformedURLException;

public abstract class BaseTest {
    private static final Logger LOG = LogManager.getLogger(BaseTest.class);

    @BeforeSuite(alwaysRun = true)
    public void configSuite() {
        LOG.info("=========Tests started=========");
    }

    @AfterSuite
    public void closeAll() throws MalformedURLException {
        LOG.info("=========Tests finished=========");
        DriverManager.getDriver().quit();
    }
}
