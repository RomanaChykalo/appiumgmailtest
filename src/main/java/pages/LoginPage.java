package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.net.MalformedURLException;

public class LoginPage extends BasePage {
    private static final Logger log = LogManager.getLogger(LoginPage.class);
    @AndroidFindBy(xpath = "//*[@resource-id='com.google.android.gm:id/welcome_tour_got_it']")
    private MobileElement gotIt;
    @AndroidFindBy(xpath = "//*[@resource-id='com.google.android.gm:id/setup_addresses_add_another']")
    private MobileElement addGmail;
    @AndroidFindBy(xpath = "//*[@text='Google']")
    private MobileElement googleGmailBtn;
    @AndroidFindBy(xpath = "//*[contains(@class, 'android.widget.EditText')]")
    private MobileElement loginInput;
    @AndroidFindBy(id = "headingText")
    private MobileElement googleIcon;
    @AndroidFindBy(xpath = "//*[@resource-id='com.google.android.gm:id/setup_fragment_content']/*[@class='android.widget.LinearLayout'][1]")
    private MobileElement personalChoose;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[1]/android.view.View[3]/android.view.View/android.view.View/android.view.View/android.view.View[3]/android.view.View")
    private MobileElement pswInput;
    @AndroidFindBy(id = "identifierNext")
    private MobileElement loginNextBtn;
    @AndroidFindBy(id = "passwordNext")
    private MobileElement pswNextBtn;
    @AndroidFindBy(className = "android.widget.Button")
    private MobileElement acceptConditionsBtn;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[1]/android.view.View/android.view.View[3]/android.view.View/android.view.View[2]")
    private MobileElement dubplicatedAccountWidget;
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='Gmail']")
    private MobileElement gmailIcon;
    @AndroidFindBy(id = "headingText")
    private MobileElement greetingIcon;
    @AndroidFindBy(id = "com.google.android.gm:id/action_done")
    private MobileElement goToGmailBtn;

    public LoginPage() throws MalformedURLException {
    }

    public MobileElement getPswNextBtn() {
        return pswNextBtn;
    }

    public MobileElement getAcceptConditionsBtn() {
        return acceptConditionsBtn;
    }

    public MobileElement getGreetingIcon() {
        return greetingIcon;
    }


    public MobileElement getGoToGmailBtn() {
        return goToGmailBtn;
    }

    public MobileElement getGotIt() {
        return gotIt;
    }

    public MobileElement getAddGmail() {
        return addGmail;
    }

    public MobileElement getGoogle() {
        return googleGmailBtn;
    }

    public MobileElement getLoginInput() {
        return loginInput;
    }

    public MobileElement getPswInput() {
        return pswInput;
    }

    public void setNext() {
        loginNextBtn.click();
    }
    public MobileElement getGoogleIcon() {
        return googleIcon;
    }

    public boolean isAccountDublicated() {
        return dubplicatedAccountWidget.isDisplayed();
    }

    public void tabGotItBtn() {
        MobileElement gotItBtn = getGotIt();
        gotItBtn.tap(1, 2);
        log.info("Click GotIt button");
    }

    public void addNewEmailToGmailList() {
        MobileElement addEmailBtn = getAddGmail();
        addEmailBtn.click();
        log.info("Click add new user email button");
    }

    public void clickGoogleBtn() {
        MobileElement googleBtn = getGoogle();
        googleBtn.click();
        log.info("Click on google button");
    }
    public void clickGoogleIcon() {
        MobileElement googleIcon =getGoogleIcon();
        googleIcon.click();
        log.info("Click on google icon");
    }

    public void goToGmailBtnClick() {
        MobileElement goToGmailBtn = getGoToGmailBtn();
        goToGmailBtn.click();
        log.info("Click on go to gmail btn");
    }

    public void loginPrecondition(){
        tabGotItBtn();
        log.info("Add new user to google mail-list");
        addNewEmailToGmailList();
        log.info("Add address to google mail");
        clickGoogleBtn();
        log.info("Click on google icon");
        wait.until(ExpectedConditions.visibilityOf(getLoginInput()));
    }

    public void enterLogin(String login) {
        MobileElement loginInput = getLoginInput();
        loginInput.sendKeys(login);
        log.info("Enter an email");
    }

    public void enterPassword(String psw) {
        MobileElement pswInput = getPswInput();
        pswInput.sendKeys(psw);
        log.info("Enter a password");
    }

    public void clickOnGreetingIcon() {
        MobileElement greetingIcon = getGreetingIcon();
        greetingIcon.click();
        log.info("Click on greeting icon");
    }

    public void acceptConditions() {
        MobileElement acceptConditionBtn = getAcceptConditionsBtn();
        acceptConditionBtn.click();
        log.info("Accepted gmail conditions");
    }

    public void setPaswdNext() {
        MobileElement paswdNextBtn = getPswNextBtn();
        paswdNextBtn.click();
        log.info("Click next button");
    }

    public void doubleBackClick() {
        driver.navigate().back();
        driver.navigate().back();
    }
}
