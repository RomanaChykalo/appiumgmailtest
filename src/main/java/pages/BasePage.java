package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.DriverManager;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class BasePage {
    protected AppiumDriver<MobileElement> driver;
    protected WebDriverWait wait;

    public BasePage() throws MalformedURLException {
        driver = DriverManager.getDriver();
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(new AppiumFieldDecorator(driver, 10, TimeUnit.SECONDS), this);
    }
}
