package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.util.List;

public class HomePage extends BasePage {
    private static final Logger log = LogManager.getLogger(HomePage.class);
    @AndroidFindBy(id = "com.google.android.gm:id/subject_and_folder_view")
    private MobileElement letterSubject;
    @AndroidFindBy(id = "com.google.android.gm:id/subject_and_folder_view")
    private MobileElement msgHeader;
    @AndroidFindBy(id = "com.google.android.gm:id/gm_dismiss_button")
    private MobileElement dismissBtn;
    @AndroidFindBy(xpath = "(//android.view.View[@content-desc=\"Вибрати цей ланцюжок повідомлень\"])[1]")
    private MobileElement chooseMessageButton;
    @AndroidFindBy(xpath = "//android.widget.ImageView[@content-desc=\"Інші варіанти\"]")
    private MobileElement anotherOptionsBtn;
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[4]/android.widget.LinearLayout")
    private MobileElement markAsImportantBtn;
    @AndroidFindBy(className = "android.view.View")
    private List<MobileElement> inputLettersList;
    @AndroidFindBy(id = "com.google.android.gm:id/sender_name")
    private MobileElement senderName;
    @AndroidFindBy(id = "com.google.android.gm:id/upper_date")
    private MobileElement letterDate;

    public HomePage() throws MalformedURLException {
    }

    public MobileElement getAnotherOptionsBtn() {
        return anotherOptionsBtn;
    }

    public MobileElement getSenderName() {
        return senderName;
    }

    public MobileElement getLetterDate() {
        return letterDate;
    }

    public List<MobileElement> getInputLettersList() {
        return inputLettersList;
    }

    public MobileElement getDismissBtn() {
        return dismissBtn;
    }

    public MobileElement getMarkAsImportantBtn() {
        return markAsImportantBtn;
    }

    public void chooseAnotherOption() {
        MobileElement anotherOptionBtn = getAnotherOptionsBtn();
        anotherOptionBtn.click();
        log.info("Clicked on another options button");
    }

    public void clickOnMarkAsImportant() {
        MobileElement markImportantBtn = getMarkAsImportantBtn();
        markImportantBtn.click();
        log.info("Clicked on mark as important button");
    }

    private void clickOnDismissBtn() {
        MobileElement dismissBtn = getDismissBtn();
        dismissBtn.click();
        log.info("Clicked on dismiss button");
    }

    public void overcomeCaptcha() {
        clickOnDismissBtn();
        clickOnDismissBtn();
        log.info("Overcome captcha");
    }

    public void openMessageFromInbox(int messNumber) {
        List<MobileElement> inputLettersList = getInputLettersList();
        MobileElement letterFromInput = inputLettersList.get(messNumber);
        letterFromInput.tap(1, 2);
        log.info(String.format("Opened %s letter from inbox", messNumber));
    }

    public String msgHeaderText() {
        return msgHeader.getText();
    }

    public void makeStepBack() {
        driver.navigate().back();
    }

}
