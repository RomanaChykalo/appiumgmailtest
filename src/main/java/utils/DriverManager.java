package utils;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverManager {
    private static AndroidDriver<MobileElement> androidDriver;
        private DriverManager() {
        }

        public static AndroidDriver getDriver() throws MalformedURLException {
            if (androidDriver == null) {
                androidDriver=initAndroidDriver();
            }
            return androidDriver;
        }

        static AndroidDriver initAndroidDriver() throws MalformedURLException {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, PropertyUtil.configList.get("device_name"));
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PropertyUtil.configList.get("platform"));
            capabilities.setCapability("appPackage", PropertyUtil.configList.get("app_package"));
            capabilities.setCapability("appActivity", PropertyUtil.configList.get("main_activity"));
            AndroidDriver<MobileElement> androidDriver = new AndroidDriver<>(new URL(PropertyUtil.configList.get("hub")), capabilities);
            androidDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            androidDriver.resetApp();
            return androidDriver;
        }

}
