package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyUtil {
    public static Map<String, String> configList = new PropertyUtil().getInfoFromPropertyFile();

    private Map<String, String> getInfoFromPropertyFile() {
        Properties prop = new Properties();
        try (InputStream input = getClass().getClassLoader().getResourceAsStream("driver.properties")) {
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        configList = new HashMap<>();
        configList.put("device_name", prop.getProperty("device_name"));
        configList.put("platform", prop.getProperty("platform"));
        configList.put("csv_path", prop.getProperty("csv_path"));
        configList.put("app_package", prop.getProperty("app_package"));
        configList.put("main_activity",prop.getProperty("main_activity"));
        configList.put("hub", prop.getProperty("hub"));
        return configList;
    }
}
