package allure;

import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import org.testng.annotations.Listeners;
import utils.DriverManager;


import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static java.lang.String.format;

@Listeners
public class CustomListener extends TestListenerAdapter {
    private File logFile = new File("test-output/log4j-Allure.log");
    public static final Logger log = LogManager.getLogger(CustomListener.class);

    @Override
    public void onStart(ITestContext context) {
        log.info("-------=======  Test \"" + context.getStartDate().toString()+ "\" started =======-------");
    }

    @Override
    public void onFinish(ITestContext context) {
        log.info("-------======= Test \"" + context.getEndDate().toString() + "\" finished =======-------");
        removeLogs();
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        log.info(format("result : SUCCESS : %s", result.getMethod().getMethodName()));
        appendLogToAllure();
    }

    @Override
    public void onTestFailure(ITestResult result) {
        log.error("result : FAILURE " + result.getMethod().getMethodName().toUpperCase());
        try {
            getScreenshot();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        appendLogToAllure();
        try {
            FileUtils.write(new File("test-output/log4j-Allure.log"), "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Attachment(value = "Test logs", type = "text/html")
    private byte[] appendLogToAllure() {
        try {
            log.info("Start read logs...........");
            Path path = Paths.get("test-output/log4j-Allure.log");
            return Files.readAllBytes(path);
        } catch (IOException e) {
            log.error("Can't read logs to Allure Report");
        }
        return null;
    }

    @Attachment(value = "screenshot", type = "image/png")
    private byte[] getScreenshot() throws MalformedURLException {
        log.info("Screenshot have been added to Allure Report.");
        return ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        log.info(result.getMethod().getMethodName() + "result : SKIPPED");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        log.info("onTestFailedButWithinSuccessPercentage for " + result.getMethod().getMethodName());
    }
    public void removeLogs() {
        try {
            FileUtils.write(logFile, "", "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
