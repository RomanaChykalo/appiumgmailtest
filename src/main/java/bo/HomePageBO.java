package bo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pages.HomePage;

import java.net.MalformedURLException;

public class HomePageBO {
    public static final Logger log = LogManager.getLogger(HomePageBO.class);
    private HomePage gmailHomePage = new HomePage();

    public HomePageBO() throws MalformedURLException {
    }

    public String markMessAsImportant(int letterNumber) {
        gmailHomePage.overcomeCaptcha();
        return setAsImportant(letterNumber);
    }

    public String getMsgHeaderText() {
        return gmailHomePage.msgHeaderText();
    }

    private String setAsImportant(int messNumber) {
        gmailHomePage.openMessageFromInbox(messNumber);
        String sender = gmailHomePage.getSenderName().getText();
        String date = gmailHomePage.getLetterDate().getText();
        String msgHeaderBeforeAction = gmailHomePage.msgHeaderText();
        gmailHomePage.chooseAnotherOption();
        log.info("Marked as important letter from: " + sender + ", time: " + date);
        gmailHomePage.clickOnMarkAsImportant();
        return msgHeaderBeforeAction;
    }


}
