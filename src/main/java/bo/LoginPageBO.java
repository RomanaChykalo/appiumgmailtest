package bo;

import pages.LoginPage;

import java.net.MalformedURLException;

public class LoginPageBO {
    private static LoginPage gmailPageObject;

    static {
        try {
            gmailPageObject = new LoginPage();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void logIn(String email, String psw) throws MalformedURLException {
        gmailPageObject.loginPrecondition();
        enterUserData(email, psw);
    }

    private void enterUserData(String username, String psw) {
        gmailPageObject.enterLogin(username);
        gmailPageObject.clickGoogleIcon();
        gmailPageObject.setNext();
        if (gmailPageObject.isAccountDublicated()) {
            gmailPageObject.doubleBackClick();
            gmailPageObject.goToGmailBtnClick();
        } else {
            gmailPageObject.enterPassword(psw);
            gmailPageObject.clickOnGreetingIcon();
            gmailPageObject.setPaswdNext();
            gmailPageObject.acceptConditions();
            gmailPageObject.goToGmailBtnClick();
        }
    }
}
